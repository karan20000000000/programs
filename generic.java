class generic
{
    static <T> void print(T[] arr)
    {
        for(T i: arr)
        {   System.out.print(i+", "); }
        System.out.println();
    }
    
    static void main()
    {
        String arr[] = {"moo","bleh","duh"};
        Integer arr2[] = {2,3,6,4};
        Double arr3[] = {2.5,7.8,1.2};
        print(arr);
        print(arr2);
        print(arr3);
        print(arr2);
    }
    
    /**static void print(int arr[])
    {
        for(int i:arr)
        {   System.out.print(i+", "); }
    }*/
}