package proj;
class recur_digitsum
{
    static int sum(int n)
    {
        if(n==0)
        {   return 0; }
        return (n%10)+sum(n/10);
    }
    static void m()
    {
        System.out.println("Sum of digits of 7683: "+sum(7638));
    }
}