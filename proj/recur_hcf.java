package proj;
class recur_hcf
{
    static int hcf(int p,int q)
    {
        if(p==0)
        {   return q;   }
        return hcf(q%p,p);
    }
    static void m()
    {
        int h = hcf(12,32);
        System.out.println("HCF of 12 and 32 is: "+h);
    }
}