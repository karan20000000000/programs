package proj;
import java.util.*;
class bubblesort
{
    public static void m()
    {
        int arr[] = {6,5,8,4,23,95,12,76,100,54,3};
        System.out.println("Original array:\n"+Arrays.toString(arr));
        sort(arr);
        System.out.println("Sorted array:\n"+Arrays.toString(arr));
    }
    static void sort(int arr[])
    {
        int t = 0;
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<(arr.length-i)-1;j++)
            {
                if(arr[j]>arr[j+1])
                {   t = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = t;
                }
            }
        }
    }
}