package proj;
class recur_strnrev
{
    static String rev(String s)
    {
        if(s.length()==1) return s;
        return rev(s.substring(1)) + "" +s.charAt(0);
    }
    static void m()
    {
        System.out.println("Reverse of philanthropist is: "+rev("philanthropist"));
    }
}