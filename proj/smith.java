package proj;
import java.util.*;
class smith
{
    static boolean isPrime(int n)
    {
        int c = 0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0) c++;
        }
        if(c<=2)
        {   return true; }
        return false;
    }
    
    static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number");
        int no = sc.nextInt();
        if(dsum(no) == psum(no))
        {   System.out.println(no+" is a smith number"); }
        else 
        {   System.out.println(no+" is not a smith number"); }
    }
    
    static int psum(int n)
    {
        if(isPrime(n))
        {   return dsum(n); }
        int num = 2;
        while(true)
        {
            if(isPrime(num) && n%num == 0)
            {  return dsum(num) + psum(n/num); }
            num++;
        }
    }
    static int dsum(int no)
    {   int sum = 0;
        int q = no;
        while(q!=0)
        {
            sum += q%10;
            q /= 10;
        }
        return sum;
    }
}
        