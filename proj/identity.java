package proj;
import java.util.*;
class identity
{
    static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter m");
        int m = sc.nextInt();
        System.out.println("Enter the matrix");
        sc.useDelimiter("\\s");
        int arr[][] = new int[m][m];
        boolean chk = true;
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<m;j++)
            {
                arr[i][j] = sc.nextInt();
                if(i!=j && arr[i][j]!=0) chk = false;
            }
        }

        for(int i=0;i<m;i++)
        {
            if(arr[i][i] != 1)
            { chk = false; }
        }

        if(chk==false)
        {   
            System.out.println("Given matrix is not identity matrix"); 
        }
        else
        {
            System.out.println("Given matrix is identity matrix"); 
        }
    }
}