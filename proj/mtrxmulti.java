package proj;
import java.util.*;
class mtrxmulti
{
    public static void m()
    {
        Scanner sc = new Scanner(System.in);
         
        System.out.print("Enter no. of rows of 1st Matrix : ");
        int r1=sc.nextInt();
        System.out.print("Enter no. of columns of 1st Matrix : ");
        int c1=sc.nextInt();
         
        System.out.print("Enter no. of rows of 2nd Matrix : ");
        int r2=sc.nextInt();
        System.out.print("Enter no. of columns of 2nd Matrix : ");
        int c2=sc.nextInt();
         
        if(c1 != r2) 
        {
            System.out.println("matrices of given order can't be multiplied");
        }
        else
        {   sc.useDelimiter("\\s");
            int arr1[][] = new int[r1][c1];
            int arr2[][] = new int[r2][c2];
            System.out.println("Enter the elements of 1st matrix");
            for(int i=0;i<arr1.length;i++)
            {
                for(int j=0;j<arr1[i].length;j++)
                {
                    arr1[i][j] = sc.nextInt();
                }
            }
            System.out.println("Enter elements of 2nd matrix");
            for(int i=0;i<arr2.length;i++)
            {
                for(int j=0;j<arr2[i].length;j++)
                {
                    arr2[i][j] = sc.nextInt();
                }
            }
            
            int arr3[][] = new int[r1][c2];
            for(int i=0;i<arr3.length;i++)
            {
                for(int j=0;j<arr3[i].length;j++)
                {   int sum = 0;
                    for(int m = 0;m<c1;m++)
                    { sum += arr1[i][m] * arr2[m][j]; }
                    arr3[i][j] = sum;
                }
            }
            
            for(int i=0;i<arr3.length;i++)
            {   System.out.println();
                for(int j=0;j<arr3[i].length;j++)
                {   System.out.print(arr3[i][j]+"\t"); }
            }
        }
    }
}