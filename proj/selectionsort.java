package proj;
import java.util.*;
class selectionsort
{
    public static void m()
    {
        int arr[] = {6,5,8,4,23,95,12,76,100,54,3};
        System.out.println("Original array:\n"+Arrays.toString(arr));
        
        int t=0;
        for(int i=0;i<arr.length;i++)
        {   t = i;
            for(int j=i+1;j<arr.length;j++)
            {   if(arr[t]>arr[j])
                {   t = j; }
            }
            int temp = arr[i];
            arr[i] = arr[t];
            arr[t] = temp;
        }
        System.out.println("Sorted array:\n"+Arrays.toString(arr));
    }
}