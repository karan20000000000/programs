package proj;
import java.util.*;
class sortedcol
{
    public static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter m");
        int m = sc.nextInt();
        int arr[][] = new int[m][m];
        sc.useDelimiter("\\s");
        System.out.println("Enter elements");
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<arr.length;j++)
            {
                arr[j][i] = sc.nextInt();
            }
        }
        
        for(int i=0;i<arr.length;i++)
        {
            sort(arr[i]);
        }
        for(int i=0;i<arr.length;i++)
        {   System.out.println();
            for(int j=0;j<arr.length;j++)
            {
                System.out.print(arr[j][i]+"\t");
            }
        }
    }
    static void sort(int arr[])
    {
        int t = 0;
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<(arr.length-i)-1;j++)
            {
                if(arr[j]>arr[j+1])
                {   t = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = t;
                }
            }
        }
    }
}