package proj;
import java.util.*;
class ascfreq
{
    public static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the sentence");
        String s = sc.nextLine();
        String arr[] = s.split("\\s|\\.|\\?|\\,|\\!");
        int num[] = new int[arr.length];
        Arrays.fill(num,1);
        for(int i=0;i<arr.length;i++)
        {
            for(int j=i+1;j<arr.length;j++)
            {
                if(arr[i].equalsIgnoreCase(arr[j]))
                {
                    num[i]++;
                    arr[j] = "";
                }
            }
        }
        sort(arr,num);
        for(int i=0;i<arr.length;i++)
        {
            if(arr[i].matches("[a-z|A-Z]+"))
            System.out.println(arr[i]+"\t\t"+num[i]);
        }
    }
    static void sort(String[] arr,int[] num)
    {
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<(arr.length-i)-1;j++)
            {
                if(num[j]>num[j+1])
                {
                    int t = num[j];
                    num[j] = num[j+1];
                    num[j+1] = t;
                    String temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }
}
