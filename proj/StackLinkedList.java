package proj;
import java.util.*;
class StackLinkedList
{
    static Employee start;
    static Employee temp;
    public static void main()
    {
        Scanner sc = new Scanner(System.in);
        start = new Employee();
        temp = new Employee();
        int ec; char ch;
        start = null;
        do
        {
            System.out.println("Enter employee number");
            ec = sc.nextInt();
            Employee newnode = new Employee();
            newnode.eno = ec;
            newnode.next = null;
            if(start == null)
            {
                start = newnode;
            }
            else
            {
                newnode.next = start;
                start = newnode;
            }
            System.out.println("y to add more nodes");
            ch = sc.next().charAt(0);
        }while(ch=='y');

        temp = start;
        int count = 0;
        while(temp!=null)
        {   count++;
            System.out.println(temp.eno);
            temp = temp.next;
        }
        
    }
    
    static int pop()
    {
        int val = 0;
        if(start!=null)
        {
            val = start.eno;
            start = start.next;
        }
        return val;
    }
}
