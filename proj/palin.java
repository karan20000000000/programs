package proj;
import java.util.*;
class palin
{
  public static void m()
  {
    Scanner sc = new Scanner(System.in);
    String s = "";
    System.out.println("Enter the string");
    s = sc.nextLine();
    StringTokenizer ob = new StringTokenizer(s);
    String arr1[] = new String[ob.countTokens()];
    String arr2[] = new String[ob.countTokens()];
    for(int i=0;ob.hasMoreTokens();i++)
    {
      arr1[i] = ob.nextToken();
      arr2[i] = rev(arr1[i]);
    }
    System.out.println("Palindromic words: ");
    for(int i=0; i<arr1.length;i++)
    {
      if((arr1[i]).equalsIgnoreCase(arr2[i]))
      { System.out.print(arr1[i]+", "); }
    }
  }
  public static String rev(String s)
  {
    String str = "";
    for(int i=s.length()-1;i>=0;i--)
    {
      str = str + "" + Character.toString(s.charAt(i));
    }
    return str;
  }

}
