package proj;
import java.util.*;
class insertionsort
{
    public static void m()
    {
        int arr[] = {6,5,8,4,23,95,12,76,100,54,3};
        System.out.println("Original array:\n"+Arrays.toString(arr));
        sort(arr);
        System.out.println("Sorted array:\n"+Arrays.toString(arr));
    }

    static void sort(int arr[])
    {
        for(int i=1;i<arr.length;i++)
        {
            int data = arr[i];
            int j=i;
            while(j>0 && arr[j-1]>data)
            {
                arr[j] = arr[j-1];
                j--;
            }
            arr[j] = data;
        }
    }
}