package proj;
class recur_factorial
{
    static int fact(int n)
    {
        if(n<=2)
        {   return 2; }
        return n*fact(n-1);
    }
    public static void m()
    {
        int d = fact(5);
        System.out.println(d);
    }
}