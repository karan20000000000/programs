package proj;
import java.util.*;
class alphapara
{
    public static void main()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the string");
        String s = sc.nextLine();
        StringTokenizer ob = new StringTokenizer(s,"!?.",true);
        String st = "";
        while(ob.hasMoreTokens())
        {
          String w = ob.nextToken().trim();
          st = st + sort(w) + (isChar(w.charAt(0))?" ":"");
        }
        System.out.println(st.trim());
    }
    static String sort(String s)
    {
        String arr[] = s.split(" "); 
        int t; String temp ="";
        for(int i=0;i<arr.length;i++)
        {
            t = i;
            for(int j=i+1;j<arr.length;j++)
            {   if(arr[j].compareToIgnoreCase(arr[t])<0)
                {  t = j;  }
            }
            
            temp = arr[i];
            arr[i] = arr[t];
            arr[t] = temp;
        }
        String st = "";
        for(String o:arr) { st = st + o +" "; }
        return st.trim();
    }
    static boolean isChar(char w)
    {
        switch(w)
        {
            case '?': case '!': case '.': case ',': return true;
        }
        return false;
    }
}
