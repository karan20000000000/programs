package proj;
class recur_lcm
{
    static int hcf(int p,int q)
    {
        if(p==0)
        {   return q;   }
        return hcf(q%p,p);
    }
    static void m()
    {
        int h = hcf(12,32);
        int lcm = (12*32)/h;
        System.out.println("LCM of 12 and 32 is: "+lcm);
    }
}