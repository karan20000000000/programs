package proj;
import java.util.*;
class primemtrx
{
    static int p = 2;
    public static void main()
    {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter m and n");
      int m = sc.nextInt();
      int n = sc.nextInt();
      int arr[][] = new int[m][n];
      
      for(int i=0;i<n;i++)
      {
          for(int j=0;j<m;j++)
          {
              arr[j][i] = nextPrime();
          }
      }
      
       for(int i=0;i<m;i++)
        {
            System.out.println();
            for(int j=0;j<n;j++)
            {
                System.out.print(arr[i][j]+"\t");
            }
        }
        
      p = 2;
    }
    static boolean isPrime(int n)
    {
        int c = 0;
        for(int i=1;i<=n;i++)
        {   if(n%i==0) c++; }
        if(c<=2) return true;
        return false;
    }
    static int nextPrime()
    {
        int i;
        for(i=p;;i++)
        {
            if(isPrime(i))
            { p = i+1; return i; }
        }
    }
}
