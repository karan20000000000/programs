package proj;
import java.util.*;
class Set
{
    int arr[];
    int n;
    Set()
    {
        arr = new int[100];
        n = 100;
    }

    Set(int a)
    {
        n = a;
        arr = new int[n];
    }

    void inputArray()
    {
        Scanner sc = new Scanner(System.in);
        arr = new int[n];
        System.out.println("Enter the elements"); sc.useDelimiter("\\s");
        for(int i=0;i<n;i++) arr[i] = sc.nextInt();
    }

    void display()
    {
        for(int i=0;i<n;i++)
        {
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }

    Set intersection(Set A)
    {
        String s="";
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<A.n;j++)
            {
                if(arr[i]==A.arr[j])
                {
                    s = s+arr[i]+" ";
                }
            }
        }

        String ar2[] = s.split(" ");
        int ar3[] = new int[ar2.length];
        for(int i=0;i<ar2.length;i++)
        {
            ar3[i] = Integer.parseInt(ar2[i]);
        }

        Set obj = new Set(ar3.length);
        obj.arr = ar3;
        return obj;
    }

    Set union(Set A)
    {
        Set obj = new Set();
        int idx = 0;
        for(int i=0;i<n;i++)
        {    obj.arr[idx++] = arr[i]; }

        for(int i=0;i<A.n;i++)
        {
            int chk = 0;
            for(int j=0;j<n;j++)
            {
                if(arr[j] == A.arr[i])
                {    chk = 1; }
            }
            if(chk==0)
            {  obj.arr[idx++] = A.arr[i]; }
        }
        obj.n = idx;
        return obj;
    }

    static void main()
    {
        Set A = new Set(4);

        int a[] = {1,2,3,4};
        A.arr = a;
        Set B = new Set(3);

        int b[] = {5,4,1};
        B.arr = b;
        Set C = A.intersection(B);
        C.display();
        Set D = A.union(B);
        D.display();
    }
}
