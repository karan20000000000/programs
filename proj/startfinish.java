package proj;
import java.util.*;
class startfinish
{
    static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter starting date DD/MM and finishing date DD/MM");
        sc.useDelimiter("\\s|\\/");
        int d1 = sc.nextInt(), m1 = sc.nextInt(), d2 = sc.nextInt(), m2 = sc.nextInt();
        long t1 = toseconds(d1,m1); long t2 = toseconds(d2,m2);
        long dr = t2-t1;
        System.out.println((dr/(60*60*24)));
    }

    static long toseconds(int d, int m)
    {
        long sec = 0;
        for(int i=1;i<=m-1;i++)
        {
            if(i<=7)
            {
                if(i==2){  sec += 29*24*60*60; }
                else if(i%2==0) sec += 30*24*60*60;
                else
                { 
                    sec += 31*24*60*60;
                }
            }
            else
            {
                if(i%2==0) sec += 31*24*60*60;
                else
                { 
                    sec += 30*24*60*60;
                }
            }
        }
        sec += d*24*60*60;
        return sec;
    }
}