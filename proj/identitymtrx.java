package proj;
import java.util.*;
class identitymtrx
{
    public static void m()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter m");
        int m = sc.nextInt();
        int arr[][] = new int[m][m];
        sc.useDelimiter("\\s");
        System.out.println("Enter elements");
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<arr.length;j++)
            {
                arr[i][j] = sc.nextInt();
            }
        }

        int arr2[][] = new int[m][m];
        for(int i=0;i<arr2.length;i++)
        {
            for(int j=0;j<arr2.length;j++)
            {   int sum = 0;
                for(int k = 0;k<m;k++)
                { sum += arr[i][k] * arr[k][j]; }
                arr2[i][j] = sum;
            }
        }
        
        if(Arrays.deepToString(arr).equals(Arrays.deepToString(arr2)))
        {   System.out.println("Given matrix is identity"); }
        else
        {   System.out.println("Given matrix is not identity"); }
        
    }
}